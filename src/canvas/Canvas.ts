/*
 * Canvas.ts
 * Defines a class that represents an HTML5 canvas
 * Created on 7/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as $ from 'jquery';
import { CanvasWidthError } from '../internal';
import { CanvasHeightError } from '../internal';

/**
 * An HTML5 canvas element
 */
export class Canvas {
	//fields
	/**
	 * The underlying HTML canvas element
	 */
	private _data: HTMLCanvasElement;

	/**
	 * The ID of the canvas
	 */
	private _id: string;

	/**
	 * The width of the canvas
	 */
	private _width: number;

	/**
	 * The height of the canvas
	 */
	private _height: number;

	//methods

	/**
	 * Constructs a new `Canvas` instance
	 *
	 * @param id The ID of the `<canvas>` element to capture
	 * @param width The width of the canvas
	 * @param height The height of the canvas
	 */
	constructor(id: string, width: number, height: number) {
		this._id = id;
		this.width = width;
		this.height = height;
		this._data = <HTMLCanvasElement>$(this._id).get(0);
	}

	/**
	 * Gets the ID of the `Canvas`
	 *
	 * @returns The ID of the `Canvas`
	 */
	public get id(): string {
		return this._id;
	}

	/**
	 * Gets the width of the `Canvas`
	 *
	 * @returns The width of the `Canvas`
	 */
	public get width(): number {
		return this._width;
	}

	/**
	 * Gets the height of the `Canvas`
	 *
	 * @returns The height of the `Canvas`
	 */
	public get height(): number {
		return this._height;
	}

	/**
	 * Sets the width of the `Canvas`
	 *
	 * @param newWidth The new width of the `Canvas`
	 */
	public set width(newWidth: number) {
		//validate the width
		if(newWidth >= 0) { //valid width
			this._width = newWidth;
			$(this._id).prop('width', this._width);
		} else { //invalid width
			throw new CanvasWidthError(this._id, newWidth);
		}
	}

	/**
	 * Sets the height of the `Canvas`
	 *
	 * @param newHeight The new height of the `Canvas`
	 */
	public set height(newHeight: number) {
		//validate the height
		if(newHeight >= 0) { //valid height
			this._height = newHeight;
			$(this._id).prop('height', this._height);
		} else { //invalid height
			throw new CanvasHeightError(this._id, newHeight);
		}	
	}

	/**
	 * @internal
	 * @ignore
	 */
	public get data(): HTMLCanvasElement {
		return this._data;
	}
}

//end of file
