/*
 * CanvasContext.ts
 * Defines a class that manages contextual data for a canvas
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Canvas } from '../internal';
import { FillStyle } from '../internal';
import { Color } from '../internal';
import { Gradient } from '../internal';
import { ImagePattern } from '../internal';

/**
 * Manages contextual data for a [[Canvas]]
 */
export class CanvasContext {
	//fields
	/**
	 * The context data for the managed `Canvas`
	 */
	private _data: CanvasRenderingContext2D;

	/**
	 * The current fill style for the renderer
	 */
	private _fillStyle: FillStyle;

	//methods

	/**
	 * Constructs a new `CanvasContext` instance
	 *
	 * @param canvas The `Canvas` to get the context for
	 */
	constructor(canvas: Canvas) {
		this._data = canvas.data.getContext("2d");
		this._fillStyle = null;
	}

	/**
	 * Gets the `FillStyle` for the rendering context
	 *
	 * @returns The current fill style
	 */
	public get fillStyle(): FillStyle {
		return this._fillStyle;
	}

	/**
	 * Sets the `FillStyle` for the rendering context
	 *
	 * @param style The new fill style
	 */
	public set fillStyle(style: FillStyle) {
		this._fillStyle = style; //update the field

		//and update the internal data field
		if(this._fillStyle !== null) {
			if(this._fillStyle instanceof Color) {
				this._data.fillStyle = 
					this._fillStyle.toString();
			} else if(this._fillStyle instanceof Gradient) {
				this._data.fillStyle = 
					this._fillStyle.data;
			} else if(this._fillStyle instanceof ImagePattern){
				this._data.fillStyle =
					this._fillStyle.data;
			}
		} else {
			this._data.fillStyle = null;
		}
	}

	/**
	 * @internal
	 * @ignore
	 */
	public get data(): CanvasRenderingContext2D {
		return this._data;
	}
}

//end of file
