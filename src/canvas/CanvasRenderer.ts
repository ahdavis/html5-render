/*
 * CanvasRenderer.ts
 * Defines a class that represents a canvas renderer
 * Created on 7/8/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { CanvasContext } from '../internal';
import { Canvas } from '../internal';
import { FillStyle } from '../internal';

/**
 * A canvas renderer
 */
export class CanvasRenderer {
	//fields
	/**
	 * The `Canvas` being rendered to
	 */
	private _target: Canvas;

	/**
	 * The rendering context
	 */
	private _ctxt: CanvasContext;

	//methods

	/**
	 * Constructs a new `CanvasRenderer` instance
	 *
	 * @param canvas The `Canvas` to render to
	 */
	constructor(canvas: Canvas) {
		this._target = canvas;
		this._ctxt = new CanvasContext(this._target);
	}

	/**
	 * Gets the current fill style being rendered
	 *
	 * @returns The current fill style being rendered
	 */
	public get fillStyle(): FillStyle {
		return this._ctxt.fillStyle;
	}

	/**
	 * Sets the current fill style being rendered
	 *
	 * @param style The new fill style
	 */
	public set fillStyle(style: FillStyle) {
		this._ctxt.fillStyle = style;
	}
}

//end of file
