/*
 * Color.ts
 * Defines a class that represents a CSS RGB color value
 * Created on 7/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { RGBAError } from '../internal';

/**
 * A CSS RGBA color value
 */
export class Color {
	//fields
	/**
	 * The red component of the `Color`
	 */
	private _red: number;

	/**
	 * The green component of the `Color`
	 */
	private _green: number;

	/**
	 * The blue component of the `Color`
	 */
	private _blue: number;

	/**
	 * The alpha component of the `Color`
	 */
	private _alpha: number;

	//methods

	/**
	 * Constructs a new `Color` instance
	 *
	 * @param red The red component of the `Color`
	 * @param green The green component of the `Color`
	 * @param blue The blue component of the `Color`
	 * @param alpha The alpha component of the `Color` (optional)
	 */
	constructor(red: number, green: number, 
			blue: number, alpha: number = 0xFF) {
		//validate the components
		let isInvalidColor = (c) => ((c < 0x00) || (c > 0xFF));
		if(isInvalidColor(red) || isInvalidColor(green)
			|| isInvalidColor(blue) || isInvalidColor(alpha)) {
			throw new RGBAError(red, green, blue, alpha);
		} else {
			this._red = red;
			this._green = green;
			this._blue = blue;
			this._alpha = alpha;
		}
	}

	/**
	 * Returns the color white
	 *
	 * @returns The color white
	 */
	public static get White(): Color {
		return new Color(0xFF, 0xFF, 0xFF);
	}

	/**
	 * Returns the color black
	 *
	 * @returns The color black
	 */
	public static get Black(): Color {
		return new Color(0x00, 0x00, 0x00);
	}

	/**
	 * Returns the color red
	 *
	 * @returns The color red
	 */
	public static get Red(): Color {
		return new Color(0xFF, 0x00, 0x00);
	}

	/**
	 * Returns the color green
	 *
	 * @returns The color green
	 */
	public static get Green(): Color {
		return new Color(0x00, 0xFF, 0x00);
	}

	/**
	 * Returns the color blue
	 *
	 * @returns The color blue
	 */
	public static get Blue(): Color {
		return new Color(0x00, 0x00, 0xFF);
	}

	/**
	 * Returns the color yellow
	 *
	 * @returns The color yellow
	 */
	public static get Yellow(): Color {
		return new Color(0xFF, 0xFF, 0x00);
	}

	/**
	 * Returns the color cyan
	 *
	 * @returns The color cyan
	 */
	public static get Cyan(): Color {
		return new Color(0x00, 0xFF, 0xFF);
	}

	/**
	 * Returns the color magenta
	 *
	 * @returns The color magenta
	 */
	public static get Magenta(): Color {
		return new Color(0xFF, 0x00, 0xFF);
	}

	/**
	 * Gets the red component of the `Color`
	 *
	 * @returns The red component of the `Color`
	 */
	public get red(): number {
		return this._red;
	}

	/**
	 * Gets the green component of the `Color`
	 *
	 * @returns The green component of the `Color`
	 */
	public get green(): number {
		return this._green;
	}

	/**
	 * Gets the blue component of the `Color`
	 *
	 * @returns The blue component of the `Color`
	 */
	public get blue(): number {
		return this._blue;
	}

	/**
	 * Gets the alpha component of the `Color`
	 *
	 * @returns The alpha component of the `Color`
	 */
	public get alpha(): number {
		return this._alpha;
	}

	/**
	 * Sets the red component of the `Color`
	 *
	 * @param newRed The new red component of the `Color`
	 */
	public set red(newRed: number) {
		//validate the component
		if((newRed < 0x00) || (newRed > 0xFF)) {
			throw new RGBAError(newRed, this._green, 
						this._blue, this._alpha);
		} else {
			this._red = newRed;
		}
	}

	/**
	 * Sets the green component of the `Color`
	 *
	 * @param newGreen The new green component of the `Color`
	 */
	public set green(newGreen: number) {
		//validate the component
		if((newGreen < 0x00) || (newGreen > 0xFF)) {
			throw new RGBAError(this._red, newGreen, 
						this._blue, this._alpha);
		} else {
			this._green = newGreen;
		}
	}

	/**
	 * Sets the blue component of the `Color`
	 *
	 * @param newBlue The new blue component of the `Color`
	 */
	public set blue(newBlue: number) {
		//validate the component
		if((newBlue < 0x00) || (newBlue > 0xFF)) {
			throw new RGBAError(this._red, this._green, 
						newBlue, this._alpha);
		} else {
			this._blue = newBlue;
		}
	}

	/**
	 * Sets the alpha component of the `Color`
	 *
	 * @param newAlpha The new alpha component of the `Color`
	 */
	public set alpha(newAlpha: number) {
		//validate the component
		if((newAlpha < 0x00) || (newAlpha > 0xFF)) {
			throw new RGBAError(this._red, this._green,
						this._blue, newAlpha);
		} else {
			this._alpha = newAlpha;
		}
	}

	/**
	 * Converts the `Color` to a CSS color value string
	 *
	 * @returns The CSS color value string representing the `Color`
	 */
	public toString(): string {
		return '#' + this._red.toString(16) 
				+ this._green.toString(16)
				+ this._blue.toString(16)
				+ this._alpha.toString(16);
	}
}

//end of file
