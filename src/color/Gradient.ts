/*
 * Gradient.ts
 * Defines an abstract class that represents a color gradient
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { GradientType } from '../internal';
import { Color } from '../internal';

/**
 * A color gradient
 */
export abstract class Gradient {
	//fields
	/**
	 * The gradient data
	 */
	protected _data: CanvasGradient;

	/**
	 * The type of `Gradient` this is
	 */
	protected _gradType: GradientType;

	//methods

	/**
	 * Constructs a new `Gradient` instance
	 *
	 * @param gradType The type of the `Gradient`
	 */
	constructor(gradType: GradientType) {
		this._data = null;
		this._gradType = gradType;
	}

	/**
	 * Gets the type of the `Gradient`
	 *
	 * @returns The type of the `Gradient`
	 */
	public get gradType(): GradientType {
		return this._gradType;
	}

	/**
	 * Adds a [[Color]] to the `Gradient`
	 *
	 * @param idx The position in the `Gradient` to add the `Color` to
	 * @param color The color to add
	 */
	public addColor(idx: number, color: Color): void {
		this._data.addColorStop(idx, color.toString());
	}

	/**
	 * @internal
	 * @ignore
	 */
	public get data(): CanvasGradient {
		return this._data;
	}
}

//end of file
