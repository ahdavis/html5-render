/*
 * LinearGradient.ts
 * Defines a class that represents a linear color gradient
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Gradient } from '../internal';
import { Rectangle } from '../internal';
import { GradientType } from '../internal';
import { CanvasContext } from '../internal';

/**
 * A linear color gradient
 */
export class LinearGradient extends Gradient {
	//field
	/**
	 * The bounds of the `LinearGradient`
	 */
	private _bounds: Rectangle;

	//methods

	/**
	 * Constructs a new `LinearGradient` instance
	 *
	 * @param bounds The bounds of the `LinearGradient`
	 * @param ctx The context to attach the gradient to
	 */
	constructor(bounds: Rectangle, ctx: CanvasContext) {
		super(GradientType.Linear);
		this._bounds = bounds;
		this._data = ctx.data.createLinearGradient(
					this._bounds.x,
					this._bounds.y,
					this._bounds.width,
					this._bounds.height);
	}

	/**
	 * Gets the bounds of the `LinearGradient`
	 *
	 * @returns The bounds of the `LinearGradient`
	 */
	public get bounds(): Rectangle {
		return this._bounds;
	}
}

//end of file
