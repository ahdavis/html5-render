/*
 * RadialGradient.ts
 * Defines a class that represents a radial color gradient
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Gradient } from '../internal';
import { GradientType } from '../internal';
import { Circle } from '../internal';
import { CanvasContext } from '../internal';

/**
 * A radial color [[Gradient]]
 */
export class RadialGradient extends Gradient {
	//fields
	/**
	 * The inner bounds of the `RadialGradient`
	 */
	private _innerBounds: Circle;

	/**
	 * The outer bounds of the `RadialGradient`
	 */
	private _outerBounds: Circle;

	//methods

	/**
	 * Constructs a new `RadialGradient` instance
	 * 
	 * @param inner The inner bounds of the gradient
	 * @param outer The outer bounds of the gradient
	 * @param ctx The context to attach the gradient to
	 */
	constructor(inner: Circle, outer: Circle, ctx: CanvasContext) {
		super(GradientType.Radial);
		this._innerBounds = inner;
		this._outerBounds = outer;
		this._data = ctx.data.createRadialGradient(
					this._innerBounds.x,
					this._innerBounds.y,
					this._innerBounds.radius,
					this._outerBounds.x,
					this._outerBounds.y,
					this._outerBounds.radius);
	}

	/**
	 * Gets the inner bounds of the `RadialGradient`
	 *
	 * @returns The inner bounds of the `RadialGradient`
	 */
	public get innerBounds(): Circle {
		return this._innerBounds;
	}

	/**
	 * Gets the outer bounds of the `RadialGradient`
	 *
	 * @returns The outer bounds of the `RadialGradient`
	 */
	public get outerBounds(): Circle {
		return this._outerBounds;
	}
}

//end of file
