/*
 * CanvasHeightError.ts
 * Defines an exception that is thrown when a canvas' height is set
 * to an invalid value
 * Created on 7/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * Thrown when a [[Canvas]]' height is set to an invalid value
 */
export class CanvasHeightError extends Error {
	//no fields

	/**
	 * Constructs a new `CanvasWidthError` instance
	 *
	 * @param id The ID of the canvas that triggered the error
	 * @param badHeight The invalid height that triggered the error
	 */
	constructor(id: string, badHeight: number) {
		//call the superclass constructor
		super('Invalid height ' + badHeight.toString() 
			+ ' for Canvas with ID ' + id);
	}
}

//end of file
