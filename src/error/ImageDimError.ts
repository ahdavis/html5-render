/*
 * ImageDimError.ts
 * Defines a error that is thrown when invalid dimensions 
 * are given for an image
 * Created on 7/8/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * Thrown when invalid dimensions are given for an image
 */
export class ImageDimError extends Error {
	//no fields

	//method

	/**
	 * Constructs a new `ImageDimError` instance
	 *
	 * @param w The width that triggered the error
	 * @param h The height that triggered the error
	 * @param path The path to the image
	 */
	constructor(w: number, h: number, path: string) {
		super('Invalid dimensions given for image ' + path
			+ '! (' + w.toString() + ',' 
			+ h.toString() + ')');
	}
}

//end of file
