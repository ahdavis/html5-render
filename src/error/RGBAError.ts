/*
 * RGBAError.ts
 * Defines an error that is thrown when invalid RGBA values are 
 * assigned to a color
 * Created on 7/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * Thrown when invalid RGBA values are assigned to a [[Color]]
 */
export class RGBAError extends Error {
	//no fields

	/**
	 * Constructs a new `RGBAError` instance
	 *
	 * @param r The red value assigned to the color
	 * @param g The green value assigned to the color
	 * @param b The blue value assigned to the color
	 * @param a The alpha value assigned to the color
	 */
	constructor(r: number, g: number, b: number, a: number) {
		//call the superclass constructor
		super("Invalid RGBA values: (" 
			+ r.toString() + ','
			+ g.toString() + ','
			+ b.toString() + ','
			+ a.toString() + ')');
	}
}

//end of file
