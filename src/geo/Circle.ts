/*
 * Circle.ts
 * Defines a class that represents a circle
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Shape } from '../internal';
import { Rectangle } from '../internal';
import { ShapeType } from '../internal';

/**
 * A geometric circle
 */
export class Circle extends Shape {
	//fields
	/**
	 * The x-coordinate of the `Circle`'s center
	 */
	private _x: number;

	/**
	 * The y-coordinate of the `Circle`'s center
	 */
	private _y: number;

	/**
	 * The radius of the `Circle`
	 */
	private _radius: number;

	//methods
	
	/**
	 * Constructs a new `Circle` instance
	 *
	 * @param x The x-coordinate of the `Circle`'s center
	 * @param y The y-coordinate of the `Circle`'s center
	 * @param r The radius of the `Circle`
	 */
	constructor(x: number, y: number, r: number) {
		super(ShapeType.Circle);
		this.x = x;
		this.y = y;
		this.radius = r;
	}

	/**
	 * Gets the x-coordinate of the `Circle`
	 *
	 * @returns The x-coordinate of the `Circle`
	 */
	public get x(): number {
		return this._x;
	}

	/**
	 * Gets the y-coordinate of the `Circle`
	 *
	 * @returns The y-coordinate of the `Circle`
	 */
	public get y(): number {
		return this._y;
	}

	/**
	 * Gets the radius of the `Circle`
	 *
	 * @returns The radius of the `Circle`
	 */
	public get radius(): number {
		return this._radius;
	}

	/**
	 * Gets the area of the `Circle`
	 *
	 * @returns The area of the `Circle`
	 */
	public get area(): number {
		return Math.PI * Math.pow(this._radius, 2); //pi*r^2
	}

	/**
	 * Gets the perimeter of the `Circle`
	 *
	 * @returns The perimeter of the `Circle`
	 */
	public get perimeter(): number {
		return 2 * Math.PI * this._radius; //2*pi*r
	}

	/**
	 * Sets the x-coordinate of the `Circle`
	 *
	 * @param newX The new x-coordinate of the `Circle`
	 */
	public set x(newX: number) {
		if(newX < 0) {
			this._x = 0;
		} else {
			this._x = newX;
		}
	}

	/**
	 * Sets the y-coordinate of the `Circle`
	 *
	 * @param newY The new y-coordinate of the `Circle`
	 */
	public set y(newY: number) {
		if(newY < 0) {
			this._y = 0;
		} else {
			this._y = newY;
		}
	}

	/**
	 * Sets the radius of the `Circle`
	 *
	 * @param newRadius The new radius of the `Circle`
	 */
	public set radius(newRadius: number) {
		if(newRadius < 1) {
			this._radius = 1;
		} else {
			this._radius = newRadius;
		}
	}

	/**
	 * Determines whether the `Circle` collides 
	 * with another `Shape`
	 *
	 * @param other The `Shape` to check collision with
	 *
	 * @returns Whether the `Circle` collides with `other`
	 */
	public collidesWith(other: Shape): boolean {
		//determine what "other" represents
		if(other instanceof Rectangle) { 
			let rect = other as Rectangle; //get the rectangle

			//calculate the distance between
			//the circle and the rectangle
			let cirDistX = Math.abs(this._x - rect.x);
			let cirDistY = Math.abs(this._y - rect.y);

			//directly compare the distances
			//between the circle and rectangle
			if(cirDistX > ((rect.width / 2) + this._radius)) {
				return false;
			}
			if(cirDistY > ((rect.height / 2) + this._radius)) {
				return false;
			}
			if(cirDistX <= (rect.width / 2)) {
				return true;
			}
			if(cirDistY <= (rect.height / 2)) {
				return true;
			}

			//finally, compare the shapes using
			//the Pythagorean theorem
			let distSq = (Math.pow(cirDistX 
						- (rect.width / 2), 2)
					+ Math.pow(cirDistY 
						- (rect.width / 2), 2));
			return (distSq <= Math.pow(this._radius, 2));
		} else if(other instanceof Circle) {
			//get the other circle
			let ocir = other as Circle;

			//use the Pythagorean theorem to
			//calculate the collision
			let xSqr = Math.pow(this._x - ocir._x, 2);
			let ySqr = Math.pow(this._y - ocir._y, 2);
			let rSqr = Math.pow(this.radius - ocir.radius, 2);
			return ((xSqr + ySqr) <= rSqr);
		}

		//no collision, so return false
		return false;
	}
}

//end of file
