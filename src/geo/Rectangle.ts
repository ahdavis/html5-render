/*
 * Rectangle.ts
 * Defines a class that represents a rectangle
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Shape } from '../internal';
import { ShapeType } from '../internal';
import { Circle } from '../internal';

/**
 * A geometric rectangle
 */
export class Rectangle extends Shape {
	//fields
	/**
	 * The x-coordinate of the top left corner
	 */
	private _x: number;

	/**
	 * The y-coordinate of the top left corner
	 */
	private _y: number;

	/**
	 * The width of the `Rectangle`
	 */
	private _width: number;

	/**
	 * The height of the `Rectangle`
	 */
	private _height: number;

	//methods

	/**
	 * Constructs a new `Rectangle` instance
	 *
	 * @param x The x-coordinate of the `Rectangle`
	 * @param y The y-coordinate of the `Rectangle`
	 * @param w The width of the `Rectangle`
	 * @param h The height of the `Rectangle`
	 */
	constructor(x: number, y: number, w: number, h: number) {
		super(ShapeType.Rectangle);
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
	}

	/**
	 * Gets the x-coordinate of the `Rectangle`
	 *
	 * @returns The x-coordinate of the `Rectangle`
	 */
	public get x(): number {
		return this._x;
	}

	/**
	 * Gets the y-coordinate of the `Rectangle`
	 *
	 * @returns The y-coordinate of the `Rectangle`
	 */
	public get y(): number {
		return this._y;
	}

	/**
	 * Gets the width of the `Rectangle`
	 *
	 * @returns The width of the `Rectangle`
	 */
	public get width(): number {
		return this._width;
	}

	/**
	 * Gets the height of the `Rectangle`
	 *
	 * @returns The height of the `Rectangle`
	 */
	public get height(): number {
		return this._height;
	}

	/**
	 * Gets the area of the `Rectangle`
	 *
	 * @returns The area of the `Rectangle`
	 */
	public get area(): number {
		return this._width * this._height;
	}

	/**
	 * Gets the perimeter of the `Rectangle`
	 *
	 * @returns The perimeter of the `Rectangle`
	 */
	public get perimeter(): number {
		return (2 * this._width) + (2 * this._height);
	}

	/**
	 * Sets the x-coordinate of the `Rectangle`
	 *
	 * @param newX The new x-coordinate
	 */
	public set x(newX: number) {
		if(newX < 0) {
			this._x = 0;
		} else {
			this._x = newX;
		}
	}

	/**
	 * Sets the y-coordinate of the `Rectangle`
	 *
	 * @param newY The new y-coordinate
	 */
	public set y(newY: number) {
		if(newY < 0) {
			this._y = 0;
		} else {
			this._y = newY;
		}
	}

	/**
	 * Sets the width of the `Rectangle`
	 *
	 * @param newWidth The new width
	 */
	public set width(newWidth: number) {
		if(newWidth < 1) {
			this._width = 1;
		} else {
			this._width = newWidth;
		}
	}

	/**
	 * Sets the height of the `Rectangle`
	 *
	 * @param newHeight The new height
	 */
	public set height(newHeight: number) {
		if(newHeight < 1) {
			this._height = 1;
		} else {
			this._height = newHeight;
		}
	}

	/**
	 * Determines whether this `Rectangle` collides
	 * with another `Shape`
	 *
	 * @param other The `Shape` to check collision for
	 *
	 * @returns Whether this `Rectangle` collides with `other`
	 */
	public collidesWith(other: Shape): boolean {
		//determine what the other Shape is
		if(other instanceof Circle) {
			//use the Circle's collidesWith()
			//implementation to save typing
			return other.collidesWith(this);
		} else if(other instanceof Rectangle) {
			//get the other rectangle
			let orect = other as Rectangle;

			//calculate the coordinates of 
			//the opposite corner of the 
			//two Rectangles
			let thisX2 = this._x + this._width;
			let thisY2 = this._y + this._height;
			let otherX2 = orect._x + orect._width;
			let otherY2 = orect._y + orect._height;

			//and compare the coordinates
			return ((this._x <= otherX2) 
				&& (thisX2 >= orect._x)
				&& (this._y <= otherY2) 
				&& (thisY2 >= orect._y));
		}

		//no collision, so return false
		return false;
	}
}

//end of file
