/*
 * Shape.ts
 * Defines an abstract class that represents a geometric shape
 * Created on 7/6/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { ShapeType } from '../internal';

/**
 * A geometric shape
 */
export abstract class Shape {
	//field
	/**
	 * The type of the `Shape`
	 */
	protected _shapeType: ShapeType;

	//methods

	/**
	 * Constructs a new Shape instance
	 *
	 * @param newType The type of the Shape
	 */
	constructor(newType: ShapeType) {
		this._shapeType = newType;
	}

	/**
	 * Gets the type of the `Shape`
	 *
	 * @returns The type of the `Shape`
	 */
	public get shapeType(): ShapeType {
		return this._shapeType;	
	}

	/**
	 * Gets the area of the `Shape`
	 *
	 * @returns The area of the `Shape
	 */
	public abstract get area(): number;

	/**
	 * Gets the perimeter of the `Shape`
	 *
	 * @returns The perimeter of the `Shape`
	 */
	public abstract get perimeter(): number;

	/**
	 * Determines whether the `Shape` collides
	 * with another `Shape`
	 *
	 * @param other The `Shape` to check collision with
	 *
	 * @returns Whether this `Shape` collides with the other `Shape`
	 */
	public abstract collidesWith(other: Shape): boolean;
}

//end of file
