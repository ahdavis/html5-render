/*
 * CanvasImage.ts
 * Defines a class that represents a canvas image
 * Created on 7/8/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { ImageDimError } from '../internal';


/**
 * An image displayable on a [[Canvas]]
 */
export class CanvasImage {
	//fields
	/**
	 * The image data
	 */
	private _data: HTMLImageElement;

	/**
	 * The path to the image file
	 */
	private _path: string;

	/**
	 * The width of the image
	 */
	private _width: number;

	/**
	 * The height of the image
	 */
	private _height: number;

	//methods

	/**
	 * Constructs a new `CanvasImage` instance
	 *
	 * @param path The path to the source image file
	 * @param w The width of the image
	 * @param h The height of the image
	 */
	constructor(path: string, w: number, h: number) {
		//validate the dimensions
		if((w < 1) || (h < 1)) {
			throw new ImageDimError(w, h, path);
		}

		//and initialize the fields
		this._path = path;
		this._width = w;
		this._height = h;
		this._data = new Image(w, h);
		this._data.src = this._path;
	}

	/**
	 * Gets the path to the referenced image file
	 *
	 * @returns The path to the image file
	 */
	public get path(): string {
		return this._path;
	}

	/**
	 * Gets the width of the image
	 *
	 * @returns The width of the image
	 */
	public get width(): number {
		return this._width;
	}

	/**
	 * Gets the height of the image
	 *
	 * @returns The height of the image
	 */
	public get height(): number {
		return this._height;
	}

	/**
	 * @internal
	 * @ignore
	 */
	public get data(): HTMLImageElement {
		return this._data;
	}
}

//end of file
