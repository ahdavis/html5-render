/*
 * ImagePattern.ts
 * Defines a class that represents a canvas image
 * Created on 7/8/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { PatternRepeat } from '../internal';
import { CanvasImage } from '../internal';
import { CanvasContext } from '../internal';

/**
 * A repeating image pattern
 */
export class ImagePattern {
	//fields
	/**
	 * The internal pattern data
	 */
	private _data: CanvasPattern;

	/**
	 * The [[CanvasImage]] that makes up the pattern
	 */
	private _image: CanvasImage;

	/**
	 * The repetition type of the pattern
	 */
	private _repeat: PatternRepeat;

	//methods

	/**
	 * Constructs a new `ImagePattern` instance
	 *
	 * @param image The image for the pattern
	 * @param repeat The repetition type for the pattern
	 * @param ctx The rendering context to attach the pattern to
	 */
	constructor(image: CanvasImage, repeat: PatternRepeat,
			ctx: CanvasContext) {
		this._image = image;
		this._repeat = repeat;
		this._data = ctx.data.createPattern(this._image.data,
						this.repeatString());
	}

	/**
	 * Gets the repeat type for the `ImagePattern`
	 *
	 * @returns The reptition type for the `ImagePattern`
	 */
	public get repeat(): PatternRepeat {
		return this._repeat;
	}


	/**
	 * Converts the `_repeat` field to a 
	 * `string` to be used by the HTML API
	 *
	 * @returns The `string` value of the `_repeat` field
	 */
	private repeatString(): string {
		let ret = "repeat"; //the return value

		//handle different types of repetition
		switch(this._repeat) {
			case PatternRepeat.Horiz: {
				ret = "repeat-x";
				break;
			}
			case PatternRepeat.HorizVert: {
				ret = "repeat";
				break;
			}
			case PatternRepeat.None: {
				ret = "no-repeat";
				break;
			}
			case PatternRepeat.Vert: {
				ret = "repeat-y";
				break;
			}
		}
		return ret; //return the generated string
	}

	/**
	 * @internal
	 * @ignore
	 */
	public get data(): CanvasPattern {
		return this._data;
	}
}

//end of file
