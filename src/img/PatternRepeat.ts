/*
 * PatternRepeat.ts
 * Enumerates image pattern repetition types
 * Created on 7/8/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * Indicates the repetition type of an [[ImagePattern]]
 */
export enum PatternRepeat {
	HorizVert, //repeat in both directions
	Horiz,     //repeat horizontally
	Vert,      //repeat vertically
	None       //no repetition
}

//end of file
