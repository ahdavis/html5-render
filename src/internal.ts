/*
 * internal.ts
 * Internal export file for html5-render
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//exports
export * from './canvas/Canvas';
export * from './canvas/CanvasContext';
export * from './canvas/FillStyle';
export * from './canvas/CanvasRenderer';
export * from './error/CanvasWidthError';
export * from './error/CanvasHeightError';
export * from './error/RGBAError';
export * from './error/ImageDimError';
export * from './color/Color';
export * from './color/Gradient';
export * from './color/GradientType';
export * from './color/LinearGradient';
export * from './geo/Shape';
export * from './geo/ShapeType';
export * from './geo/Rectangle';
export * from './geo/Circle';
export * from './img/CanvasImage';
export * from './img/PatternRepeat';
export * from './img/ImagePattern';

//end of file
