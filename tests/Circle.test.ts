/*
 * Circle.test.ts
 * Unit test for the Circle class
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Circle } from '../src/internal';
import { Rectangle } from '../src/internal';

//tests

//this test ensures that the Circle properly
//validates its constructor arguments
test('Constructor validates arguments', () => {
	let c = new Circle(-1, -2, 0);
	expect(c.x).toBe(0);
	expect(c.y).toBe(0);
	expect(c.radius).toBe(1);
});

//this test checks validation of the x-coordinate being set
test('X-coordinate being set', () => {
	let c = new Circle(3, 3, 4);
	expect(c.x).toBe(3);
	c.x = 7;
	expect(c.x).toBe(7);
	c.x = -1;
	expect(c.x).toBe(0);
});

//this test checks validation of the y-coordinate being set
test('Y-coordinate being set', () => {
	let c = new Circle(3, 3, 4);
	expect(c.y).toBe(3);
	c.y = 7;
	expect(c.y).toBe(7);
	c.y = -1;
	expect(c.y).toBe(0);
});

//this test checks validation of the radius being set
test('Radius being set', () => {
	let c = new Circle(3, 3, 4);
	expect(c.radius).toBe(4);
	c.radius = 5;
	expect(c.radius).toBe(5);
	c.radius = -1;
	expect(c.radius).toBe(1);
});

//this test checks collision with another Circle 
test('Collides with Circle', () => {
	let c1 = new Circle(3, 3, 4);
	let c2 = new Circle(2, 2, 7);
	expect(c1.collidesWith(c2)).toBe(true);
});

//this test checks non-collision with another Circle
test('Does not collide with Circle', () => {
	let c1 = new Circle(3, 3, 2);
	let c2 = new Circle(10, 10, 1);
	expect(c1.collidesWith(c2)).toBe(false);
});

//this test checks collision with a Rectangle
test('Collides with Rectangle', () => {
	let c = new Circle(3, 3, 4);
	let r = new Rectangle(2, 2, 10, 10);
	expect(c.collidesWith(r)).toBe(true);
});

//this test checks non-collision with a Rectangle
test('Does not collide with Rectangle', () => {
	let c = new Circle(3, 3, 4);
	let r = new Rectangle(20, 20, 3, 3);
	expect(c.collidesWith(r)).toBe(false);
});

//this test checks area calculation
test('Area calculation', () => {
	let c = new Circle(0, 0, 1);
	expect(c.area).toBe(Math.PI);
});

//this test checks perimeter calculation
test('Perimeter calculation', () => {
	let c = new Circle(0, 0, 1);
	expect(c.perimeter).toBe(2 * Math.PI);
});

//end of file
