/*
 * Color.test.ts
 * Unit test for the Color class
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { Color } from '../src/color/Color';

//tests

//this test ensures that the default alpha value is 0xFF
test('alpha default is 255', () => {
	let c = new Color(0xFF, 0xFF, 0xFF);
	expect(c.alpha).toBe(0xFF);
});

//this test verifies the toString method
test('Color::toString() works correctly', () => {
	let c = new Color(0xAA, 0x45, 0x67, 0x99);
	expect(c.toString()).toBe('#aa456799');
});

//this test verifies the white generator
test('Color::White works correctly', () => {
	let c = Color.White;
	expect(c.red).toBe(0xFF);
	expect(c.green).toBe(0xFF);
	expect(c.blue).toBe(0xFF);
	expect(c.alpha).toBe(0xFF);
});

//this test verifies the black generator
test('Color::Black works correctly', () => {
	let c = Color.Black;
	expect(c.red).toBe(0x00);
	expect(c.green).toBe(0x00);
	expect(c.blue).toBe(0x00);
	expect(c.alpha).toBe(0xFF);
});

//this test verifies the red generator
test('Color::Red works correctly', () => {
	let c = Color.Red;
	expect(c.red).toBe(0xFF);
	expect(c.green).toBe(0x00);
	expect(c.blue).toBe(0x00);
	expect(c.alpha).toBe(0xFF);
});

//this test verifies the green generator
test('Color::Green works correctly', () => {
	let c = Color.Green;
	expect(c.red).toBe(0x00);
	expect(c.green).toBe(0xFF);
	expect(c.blue).toBe(0x00);
	expect(c.alpha).toBe(0xFF);
});

//this test verifies the blue generator
test('Color::Blue works correctly', () => {
	let c = Color.Blue;
	expect(c.red).toBe(0x00);
	expect(c.green).toBe(0x00);
	expect(c.blue).toBe(0xFF);
	expect(c.alpha).toBe(0xFF);
});

//this test verifies the yellow generator
test('Color::Yellow works correctly', () => {
	let c = Color.Yellow;
	expect(c.red).toBe(0xFF);
	expect(c.green).toBe(0xFF);
	expect(c.blue).toBe(0x00);
	expect(c.alpha).toBe(0xFF);
});

//this test verifies the cyan generator
test('Color::Cyan works correctly', () => {
	let c = Color.Cyan;
	expect(c.red).toBe(0x00);
	expect(c.green).toBe(0xFF);
	expect(c.blue).toBe(0xFF);
	expect(c.alpha).toBe(0xFF);
});

//this test verifies the magenta generator
test('Color::Magenta works correctly', () => {
	let c = Color.Magenta;
	expect(c.red).toBe(0xFF);
	expect(c.green).toBe(0x00);
	expect(c.blue).toBe(0xFF);
	expect(c.alpha).toBe(0xFF);
});

//end of file
