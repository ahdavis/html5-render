/*
 * Rectangle.test.ts
 * Unit test for the Rectangle class
 * Created on 7/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Rectangle } from '../src/index';
import { Circle } from '../src/index';

//tests

//this test ensures that the Rectangle properly
//validates its constructor arguments
test('Constructor validates arguments', () => {
	let r = new Rectangle(-1, -2, 0, 0);
	expect(r.x).toBe(0);
	expect(r.y).toBe(0);
	expect(r.width).toBe(1);
	expect(r.height).toBe(1);
});

//this test checks validation of the x-coordinate being set
test('X-coordinate being set', () => {
	let r = new Rectangle(3, 3, 34, 45);
	expect(r.x).toBe(3);
	r.x = 7;
	expect(r.x).toBe(7);
	r.x = -1;
	expect(r.x).toBe(0);
});

//this test checks validation of the y-coordinate being set
test('Y-coordinate being set', () => {
	let r = new Rectangle(3, 3, 34, 45);
	expect(r.y).toBe(3);
	r.y = 7;
	expect(r.y).toBe(7);
	r.y = -1;
	expect(r.y).toBe(0);
});

//this test checks validation of the width being set
test('Width being set', () => {
	let r = new Rectangle(3, 3, 34, 45);
	expect(r.width).toBe(34);
	r.width = 23;
	expect(r.width).toBe(23);
	r.width = -1;
	expect(r.width).toBe(1);
});

//this test checks validation of the height being set
test('Height being set', () => {
	let r = new Rectangle(3, 3, 34, 45);
	expect(r.height).toBe(45);
	r.height = 34;
	expect(r.height).toBe(34);
	r.height = -1;
	expect(r.height).toBe(1);
});

//this test checks collision with another Rectangle
test('Collides with Rectangle', () => {
	let r1 = new Rectangle(3, 3, 5, 5);
	let r2 = new Rectangle(2, 2, 4, 4);
	expect(r1.collidesWith(r2)).toBe(true);
});

//this test checks non-collision with another Rectangle
test('Does not collide with Rectangle', () => {
	let r1 = new Rectangle(0, 0, 5, 5);
	let r2 = new Rectangle(10, 10, 2, 2);
	expect(r1.collidesWith(r2)).toBe(false);
});

//this test checks collision with a Circle
test('Collides with Circle', () => {
	let r = new Rectangle(0, 0, 5, 5);
	let c = new Circle(1, 1, 7);
	expect(r.collidesWith(c)).toBe(true);
});

//this test checks non-collision with a Circle
test('Does not collide with Circle', () => {
	let r = new Rectangle(0, 0, 5, 5);
	let c = new Circle(20, 20, 5);
	expect(r.collidesWith(c)).toBe(false);
});

//this test checks area calculation
test('Area calculation', () => {
	let r = new Rectangle(0, 0, 2, 2);
	expect(r.area).toBe(4);
});

//this test checks perimeter calculation
test('Perimeter calculation', () => {
	let r = new Rectangle(0, 0, 3, 4);
	expect(r.perimeter).toBe(14);
});

//end of file
